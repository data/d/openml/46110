# OpenML dataset: Australian-Electricity-Demand

https://www.openml.org/d/46110

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Australian Electricity Demand forecasting data, half-hourly data.

From the website:
-----
This dataset contains 5 time series representing the half hourly electricity demand of 5 states in Australia: Victoria, New South Wales, Queensland, Tasmania and South Australia. It was extracted from R tsibbledata package.

-----

They claim that the original data comes from the R tsibbledata package, but I could not find the original data in the package or the original source.

There are 5 columns:

id_series: The identifier of a time series.

state: The category (state) of the time series.

value: The value of the time series at 'time_step'.

time_step: The time step on the time series.

date: The reconstructed date of the time series in the format %Y-%m-%d %H:%M:$S.

Preprocessing:

1 - Renamed columns 'series_name' and 'series_value' to 'id_series' and 'value'.

2 - Exploded the 'value' column.

3 - Created 'time_step' column from the exploded data.

4 - Created 'date' column from 'starting_date' and 'time_step'.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46110) of an [OpenML dataset](https://www.openml.org/d/46110). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46110/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46110/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46110/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

